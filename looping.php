<?php
$x = 1;
while($x<=5){
    echo "the number is $x <br>";
    $x++;
}

echo "<br>";
$x = 1;
do{
    echo "the number is $x <br>";
    $x++;
}while($x<=5);

echo "<br>";
for($x=1; $x<=5; $x++){
    echo "the number is $x <br>";
}

echo "<br>";
$array = array("Jakarta", "Bandung", "Surabaya", "Yogyakarta", "Medan", "Makassar", "Banten");
foreach ($array as $value) {
    echo "$value <br>";
}

echo "<br>";
for($i=0; $i<count($array); $i++){
    echo "$array[$i]<br>";
}

echo "<br>";
foreach ($array as $key => $value) {
    echo "index ke-$key adalah $value <br>";
}
?>